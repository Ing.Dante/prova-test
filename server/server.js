const express = require("express");
const hbs = require("hbs");
const bodyParser = require("body-parser");

const port = 3000;
var app = express();
app.use(bodyParser.json()); // definizione del middleware che deve gestire la richiesta in ingresso
app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + "/../views/partials");

app.set('case sensitive routing', true);
app.use(express.static(__dirname + "/../public"));

hbs.registerHelper("getCurrentYear", () => {
  return new Date().getFullYear();
});

hbs.registerHelper("screamIt", (text) => {
  return text.toUpperCase();
});

app.get("/", (req, res) => {
  res.render("home.hbs", {
    pageTitle: "Home Page",
    welcomeMessage: "Welcome to my website"
  });
});

app.get("/asd", (req, res) => {
  res.render("home.hbs", {
    pageTitle: "Home Page ASD",
    welcomeMessage: "Welcome to my website"
  });
});


app.listen(port, () => {
  console.log(`Server is up on port ${port}`);
});


module.exports = {
  app
};
