var env = process.env.NODE_ENV || "development"; // è solo su heroku, in default (settata a "production"). Vedi package.json per dettagli su come settare

console.log("env ***", env);

if (env === "development") {
  process.env.PORT = 3000;
  process.env.MONGODB_URI = "mongodb://localhost:27017/OfficePlan";
}
else if (env === "test"){
  process.env.PORT = 3000;
  process.env.MONGODB_URI = "mongodb://localhost:27017/OfficePlanTest";
}
