const expect = require("expect");
const request = require("supertest");

const {app} = require("./../server");


describe("GET A page", () => {
  it("should just wait here for a while...", (done) => {
    setTimeout(function () {
      console.log("\tJust waiting for the server to be properly up and running...");;
      done();
    }, 500)
  });
  it("should get a page", (done) => {
    request(app)
      .get("/asd")
      .expect(200)
      .end(done);
  });
  it("should get the root page", (done) => {
    request(app)
      .get("/")
      .expect(200)
      .end(done);
  });
});
